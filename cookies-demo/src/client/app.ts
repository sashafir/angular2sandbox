import {bootstrap} from 'angular2/platform/browser';
import {enableProdMode} from 'angular2/core';
import {environment} from './app/environment';
import {CookiesDemoApp} from './app/cookies-demo';

if (environment.production) {
  enableProdMode();
}

bootstrap(CookiesDemoApp);
