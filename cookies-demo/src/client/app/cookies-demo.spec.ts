import {describe, it, expect, beforeEachProviders, inject} from 'angular2/testing';
import {CookiesDemoApp} from '../app/cookies-demo';

beforeEachProviders(() => [CookiesDemoApp]);

describe('App: CookiesDemo', () => {
  it('should have the `defaultMeaning` as 42', inject([CookiesDemoApp], (app: CookiesDemoApp) => {
    expect(app.defaultMeaning).toBe(42);
  }));

  describe('#meaningOfLife', () => {
    it('should get the meaning of life', inject([CookiesDemoApp], (app: CookiesDemoApp) => {
      expect(app.meaningOfLife()).toBe('The meaning of life is 42');
      expect(app.meaningOfLife(22)).toBe('The meaning of life is 22');
    }));
  });
});

