export class CookiesDemoPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('cookies-demo-app p')).getText();
  }
}
