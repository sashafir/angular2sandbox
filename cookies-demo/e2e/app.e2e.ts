import { CookiesDemoPage } from './app.po';

describe('cookies-demo App', function() {
  let page: CookiesDemoPage;

  beforeEach(() => {
    page = new CookiesDemoPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('cookies-demo Works!');
  });
});
